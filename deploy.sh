
RegistryImage=${REPOSITORY}/${APP}:${TAG}
docker build -t ${RegistryImage} .
docker login --username ${REPOSITORY} --password ${DOCKER_TOKEN}
docker push ${RegistryImage}

ssh admin@${SSH_HOSTS} /home/admin/deploy.sh
